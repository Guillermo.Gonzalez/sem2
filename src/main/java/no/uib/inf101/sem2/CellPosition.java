package no.uib.inf101.sem2;

public record CellPosition(int row, int col) {
}
