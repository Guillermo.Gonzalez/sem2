package no.uib.inf101.sem2;

/**
 * This interface gets implemented in the "RacingObstacles" class.
 * Makes sure that the obstacles have their specific qualities.
 */
public interface IRacingObstacles {
    /**
     * This method updates the state of the obstacle on the racing track
     * @author Guillermo Gamboa
     */
    public void updateObstacles();

    /**
     * This method takes an object of type "RacingCar" in the parameter and checks if the racing car collides
     * with other racing cars on the track.
     * @param racingCar
     * @return true (if collision), false otherwise
     */
    public boolean collidesWith(RacingCar racingCar);

    /**
     * This method takes an object of type CarGraphics (NOT IMPLEMENTED YET) as a parameter
     * and draws the various obstacles on the screen.
     * @param graphics
     */
    public void draw(CarGraphics graphics);

    /**
     * This method chooses a random position along the vertical axis on the racing track
     * giving the maximum position (Height) by the parameter.
     * @param maxPos
     */
    public void setRandomObstaclePos(int maxPos);

    /**
     * This method takes care of the dimension of the different obstacles.
     * Taking the minimum/maximum ranges given as a parameter.
     * @param minWidth
     * @param maxWidth
     * @param minHeight
     * @param maxHeight
     */

    public void setSize(int minWidth,int maxWidth, int minHeight, int maxHeight);

    /**
     * This method makes an obstacle and puts it on the map!
     */
    public void setType();
}
