package no.uib.inf101.sem2.Controller;

public interface ICarController {

    boolean moveCar(int deltaRow, int deltaCol);

    GameState getGameState();
}
