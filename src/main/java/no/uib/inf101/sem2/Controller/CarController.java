package no.uib.inf101.sem2.Controller;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;


public class CarController implements KeyListener {

    ControllableCarModel controllableCar;
    GuillermosNitroRushView gameView;

    public CarController(ControllableCarModel controllableCar, GuillermosNitroRushView gameView){
        gameView.setFocusable(true);
        this.controllableCar = controllableCar;
        this.gameView = gameView;
        gameView.addKeyListener(this);
    }


    //@Override
    //public void keyTyped(KeyEvent e) {

    //}

    @Override
    public void keyPressed(KeyEvent e) {

        if (e.getKeyCode()== KeyEvent.VK_LEFT){
            // Moves the car to the left
            controllableCar.moveCar(-1,0);
        }
        else if (e.getKeyCode()==KeyEvent.VK_RIGHT){
            controllableCar.moveCar(1,0);
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

        if (e.getKeyCode() == KeyEvent.VK_LEFT){
            controllableCar.stopMoving();
        }

        else if (e.getKeyCode() == KeyEvent.VK_RIGHT){
            controllableCar.stopMoving();
        }

    }
}
