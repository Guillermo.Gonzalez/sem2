package no.uib.inf101.sem2;

/**
 * This class represents the player's racing car in the game.
 * @author Guillermo Gamboa
 */
public class RacingCar implements IRacingCar{

    //Variables
    public int xPos; // Position of racing car on X axis // MAKE PRIVATE (all)
    public int yPos; // Position of racing car on Y axis
    public int speed; // speed of the racing car
    public int dir; // Direction of racing car

    //Constructor
    public RacingCar(int xPos, int yPos, int speed, int dir){
        this.xPos = xPos;
        this.yPos = yPos;
        this.dir = dir;
    }

    /**
     * Method that creates and gets the position of the racing car on x-axis.
     * @return Position of the racing car on x-axis.
     */
    public int getXPos(){
        return xPos;
    }

    /**
     * Method that creates and gets the position of the racing car on y-axis
     * @return the position of the racing car on the y-axis
     */
    public int getYPos(){
        return yPos;
    }

    /**
     * Method that gets the direction of the racing car.
     * @return the direction of the racing car.
     */
    public int getDir(){
        return dir;
    }

    @Override
    public void moveLeft() {
        xPos -= speed;
        dir = -1;
    }

    @Override
    public void moveRight() {
        xPos += speed;


    }
    @Override
    public void setSpeedOfCar(int speedOfCar) {
        this.speed = speedOfCar;
    }
}
