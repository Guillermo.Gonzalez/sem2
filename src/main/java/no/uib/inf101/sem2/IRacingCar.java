package no.uib.inf101.sem2;

/**
 * This interface class gets implemented in the RacingCar class
 * and helps us create a racing car with the given qualities of it.
 * @author Guillermo Gamboa
 */
public interface IRacingCar {

    /**
     * Makes the racing car turn to the right.
     */
    void moveLeft();

    /**
     * Makes the racing car turn to the left.
     */
    void moveRight();

    /**
     * Sets the speed of the racing car, taking it as a parameter.
     * @param speedOfCar
     */
    void setSpeedOfCar(int speedOfCar);

}
