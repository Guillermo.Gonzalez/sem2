package no.uib.inf101.sem2;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

/**
 * This class represents the various obstacles on theracing track.
 * Generates random position for the obstacles, the size of
 * @author Guillermo Gamboa
 */
public class RacingObstacles implements IRacingObstacles {

    //Variables
    private int xPos;
    private int yPos;
    private int width;
    private int height;
    private Color obstacleColor;
    private Shape obstacleShape;

    //Constructor
    public RacingObstacles(int xPos, int yPos, int width, int height){
        this.xPos = xPos;
        this.yPos = yPos;
        this.width = width;
        this.height = height;
    }


    @Override
    public void updateObstacles() {

    }

    @Override
    public boolean collidesWith(RacingCar racingCar) {
        return false;
    }

    @Override
    public void draw(CarGraphics graphics) {

    }

    @Override
    public void setRandomObstaclePos(int maxPos) {

    }

    @Override
    public void setSize(int minWidth, int maxWidth, int minHeight, int maxHeight) {

    }

    @Override
    public void setType() {
        int obstacle = (int) (Math.random() * 2);
        switch (obstacle){
            case 0: // case 0 creates an orange traffic cone as an obstacle on the track.
                this.obstacleColor = Color.ORANGE;
                this.obstacleShape = new Rectangle2D.Double(xPos,yPos, width, height);
                break; // exits the switch block after creating the obstacle
            case 1:
                this.obstacleColor = Color.GRAY;
                this.obstacleShape = new Ellipse2D.Double(xPos,yPos,width,height);
                break; //
            default: // Illegal argument
        }

    }
}
