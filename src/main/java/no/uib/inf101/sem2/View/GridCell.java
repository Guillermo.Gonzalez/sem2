package no.uib.inf101.sem2.View;

import main.CellPosition;

public record GridCell<E>(CellPosition position, E value) {
}
