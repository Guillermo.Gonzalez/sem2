package no.uib.inf101.sem2.View;

import main.CellPosition;
import main.IGrid;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Grid<E> implements IGrid<E> {

    private final int rows;

    private final int cols;

    private final List<List<E>> grid;

    public Grid(int rows, int cols){
        this(rows,cols,null);
    }

    public Grid(int rows, int cols, E defaultValue){
        this.rows = rows;
        this.cols = cols;
        this.grid = new ArrayList<>(rows);

        for (int r = 0; r < rows; r++) {
            this.grid.add(new ArrayList<>(cols));
            for (int c = 0; c < cols; c++) {
                this.grid.get(r).add(defaultValue);
            }
        }
    }
    @Override
    public void set(CellPosition pos, E value) {
        this.grid.get(pos.row()).set(pos.col(), value);
    }

    @Override
    public E get(CellPosition pos) {
        return this.grid.get(pos.row()).get(pos.col());
    }

    @Override
    public boolean positionIsOnGrid(CellPosition pos) {
        return pos.row() >= 0 && pos.row() < this.rows && pos.col() >= 0 && pos.col() < this.cols;
    }

    @Override
    public Iterator<GridCell<E>> iterator() {
        List<GridCell<E>> copyList = new ArrayList<>();
        for (int r = 0; r < this.rows; r++) {
            for (int c = 0; c < this.cols; c++) {
                CellPosition position = new CellPosition(r,c);
                copyList.add(new GridCell<>(position, this.get(position)));
            }
        }
        return copyList.iterator();
    }

    @Override
    public int rows() {
        return this.rows;
    }

    @Override
    public int cols() {
        return this.cols;
    }
}
