package no.uib.inf101.sem2.View;

public interface GridDimension {
    /** Number of rows in the grid */
    int rows();

    /** Number of columns in the grid */
    int cols();

}
